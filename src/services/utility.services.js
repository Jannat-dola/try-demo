import Axios from "axios/index";

export function setToken(token) {
    localStorage.setItem('accessToken', token);
}

export function getToken() {
    return localStorage.getItem('accessToken');
};

export function getHeaderOptions() {
    const accessToken = getToken();
    /*headers.append('Authorization', 'Bearer ' + accessToken);
    headers.append('Access-Control-Allow-Origin', '*');*/

    return {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    }
};


let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2VhdGxcL2RpYS1wcm9qZWN0XC9kaWEtYmFja2VuZFwvcHVibGljXC9hcGlcL2xvZ2luIiwiaWF0IjoxNTM2MTI1MTAwLCJuYmYiOjE1MzYxMjUxMDAsImp0aSI6ImpoSWtkenBMU0M0VDdVZmYiLCJzdWIiOjIwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.C-hyK1e5g7KM_kvISNsesGm1Od0ucnOzBWjLvGnfpFg';
let b64DecodeUnicode = str =>
    decodeURIComponent(
        Array.prototype.map.call(atob(str), c =>
            '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
        ).join(''))

let parseJwt = token =>
    JSON.parse(
        b64DecodeUnicode(
            token.split('.')[1].replace('-', '+').replace('_', '/')
        )
    )

export function getAuth() {
    // just decode 'token' into {header: Object, payload: Object, signature: String}
    alert('hello');
    const parsedJWTToken = this.parseJwt(token);
    console.log(parsedJWTToken);
    return parseJwt

}


export function getUserName() {
    var token = this.getToken();       
    if(token)
    {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        return JSON.parse(window.atob(base64));
    }
    else
        return 'Admin';
        
}

export function getUserInfo()
{
   var token = this.getToken();    
    if(token)
    {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        return JSON.parse(window.atob(base64));
    }
    else
    {
        return [];
    } 
}

