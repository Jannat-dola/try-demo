import Vue from "vue";
import Vuex from "vuex";

import common from "./module/common";
import item from "./module/item";
import login from "./module/login";
import user from "./module/user";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: { common, item, login, user }
});

export default store;
