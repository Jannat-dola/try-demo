import Axios from "axios";

import {Userinfo_FetchUrl, Options, Userinfo_DeleteUrl, Userinfo_UpdateUrl, Userinfo_SaveUrl} from '../api' 

import {
 SET_USER_DATA,
 SET_USER_SCHEMA_DATA,
 SET_USER_SAVE_DATA,
} from "../mutation-types";

 const state = {

 	   schema: [{
        id: "",
        user_name: "",
        email: "",
        password: "",
        date: "",
        user_type: "",
        created_at: "",
        updated_at: "",

  }],
   userlist: [{
  }]


 };

 const getters ={
  getUserApiData(state) {
    return state.schema;
  },
  getUserList(state){
    return state.userlist;
  }
 };

const mutations = {
  [SET_USER_DATA] (state, tableData){
      state.userlist = tableData
    },
    [SET_USER_SCHEMA_DATA] (state, schemaData){
      state.schema = schemaData
    },
  [SET_USER_SAVE_DATA](state, schemaData) {
    state.userlist.push(schemaData);
  },

};

const actions ={
  
  //-------get user data from api
    callUserApiData({commit},schema){
         
           return new Promise(function(resolve, reject){

            Axios.get(Userinfo_FetchUrl,Options).then(function (response) {
              console.log("response", response )
              if ( response.data.status == "success"){

              var result = response.data.data
              commit("SET_USER_DATA", result)          
               resolve()
               }
            })
            .catch(function (error) {
              // handle error
              console.log(error);
              reject()
                 })
              });
                  
        },
    //--------save new data
    saveUserData({ commit }, schema) {
    return new Promise((resolve, reject) => {
     
      var params = new URLSearchParams();
    params.append("user_name", schema.user_name);
    params.append("email", schema.email);
    params.append("password", schema.password);
    params.append("date", schema.date);
    params.append("user_type", schema.user_type);

    Axios.post(Userinfo_SaveUrl, params, Options)
      .then(function(response) {
        if (response.data.status == "success") {
          commit("SET_USER_SAVE_DATA", schema);
          resolve();
        } else {
          //commit("SET_MESSAGE", response.data.msg);
          reject();
        }
      })
      .catch(function(error) {
        console.log(error);
        reject();
      });
  });

  },
  //----edit user Axios.post(api2+'/',params)
    editUserData({ commit, state }, schema ) {
      console.log("id", schema.item)
    return new Promise(function(resolve, reject) {
    const editurl = `${Userinfo_UpdateUrl}/${schema.item.id}`;
    var params = new URLSearchParams();
    params.append("user_name", schema.item.user_name);
    params.append("email", schema.item.email);
    params.append("password", schema.item.password);
    params.append("date", schema.item.date);
    params.append("user_type", schema.item.user_type);

     
      Axios.put(editurl, params, Options).then(function(response) {
        if (response.data.status == "success") {
         state.userlist.splice(schema.indexEdited, 1, schema.item)
      resolve();
        } else {
          //commit("SET_MESSAGE", response.data.msg);
          reject();
        }
      })
      .catch(function(error) {
        console.log(error);
        reject();
      });
    });
  },
    //delete user
      userdeleteItem({ commit, state }, deleteditem) {
      return new Promise(function(resolve, reject) {
      const deleteurl = `${Userinfo_FetchUrl}/${deleteditem}`; 
      // var params = new URLSearchParams();
      // params.append("id", deleteditem);
      Axios.delete(deleteurl,Options).then(function(response) {
        if (response.data.status == "success") {
        // state.userlist.splice(deleteditem, 1);
      resolve();
        } else {
          //commit("SET_MESSAGE", response.data.msg);
          reject();
        }
      })
      .catch(function(error) {
        console.log(error);
        reject();
      });
    });
  },

};

 export default {
  state,
  getters,
  actions,
  mutations
};
