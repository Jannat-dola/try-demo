import Axios from "axios";
import {api1} from '../api' 

import {
  SET_ITEM_DATA,
  ADD_ITEM_DATA,
  DELETE_ITEM_DATA,
  SET_FORM_DATA,
  SET_FORM_SCHEMA,
  UNSET_FORM_SCHEMA,
  SET_MESSAGE,
  UNSET_MESSAGE,
  SET_API_DATA
} from "../mutation-types";

import {
  saveMsg,
  failedMsg,
  deleteMsg ,
  updateMsg
} from "../erpUtil";

const state = {
  message: '',
  schema: {
    item_id: "",
    item_name: "",
    item_type: "",
    code: "",
    brand: ""
  },

  apidata: [{
        user_id: "",
        user_name: "",
        email: "",
        password: "",
        date: "",
        user_type: "",
        created_at: "",
        updated_at: "",

  }],
  datalist: [
    {
      item_id: 1,
      item_name: "Pencil",
      item_type: "Stationary",
      code: "001",
      brand: "Matador"
    },
    {
      item_id: 2,
      item_name: "Pen",
      item_type: "Stationary",
      code: "002",
      brand: "Orbit"
    }
  ]
};

const getters = {
  getItem(state) {
    return state.datalist;
  },
  getSchema(state) {
    return state.schema;
  },
   getItemMessage(state) {
    return state.message;
  },
   getApiData(state) {
    return state.apidata;
  },
};
const mutations = {
  [SET_ITEM_DATA](state) {
    state.datalist;
  },
  [SET_FORM_DATA](state, jschema) {
    state.datalist.push(jschema);
  },
   [SET_FORM_SCHEMA] (state, schemaData) {
      state.schema = schemaData
  },
   [UNSET_FORM_SCHEMA] (state, schemaData) {
      state.schema = {
        item_id: "",
        item_name: "",
        item_type: "",
        code: "",
        brand: ""
      }
  },
  [SET_MESSAGE](state, Msg) {
    state.message = Msg;
  },
  [UNSET_MESSAGE](state) {
    state.message = "";
  },
   [SET_API_DATA] (state, tableData){
      state.apidata = tableData
    },
};
const actions = {
  callItemData({ commit }) {
    commit("SET_ITEM_DATA");
  },
  calldeleteItem({ commit, state }, deleteditem) {
    console.log(" In action ", deleteditem);
    state.datalist.splice(deleteditem, 1);
    commit("SET_MESSAGE", deleteMsg);
  },

  saveData({ commit, state }, schema) {

    console.log("Schema - action  :", schema);

    return new Promise(function(resolve, reject) {
      //axios call 

      commit("SET_MESSAGE",saveMsg);
      commit("SET_FORM_DATA", schema);
      resolve();
    });
  },

    callUpdateData({commit},schema){
           return new Promise(function(resolve, reject){

            Axios.get(api1).then(function (response) {
              console.log("response", response )
              if ( response.data.status == "success"){

              var result = response.data.data
              commit("SET_API_DATA", result)
              
              // handle success
               
               resolve()
               }
            })
            .catch(function (error) {
              // handle error
              console.log(error);
              reject()
                 })
              });
                  
                },
};

export default {
  state,
  getters,
  actions,
  mutations
};
