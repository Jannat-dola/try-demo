import Axios from "axios";
import {Userinfo_LoginUrl} from '../api' 
import {
  SET_LOGIN_DATA
} from "../mutation-types";

const state = {
	schema: {
      name:"",
	    email: "",
	    password: "",
      confrimpassword: "",
	  },
};
const getters = {
	getLoginSchema(state) {
    return state.schema;
  },
};
const mutations = {
	[SET_LOGIN_DATA](state, data) {
    state.schema = data;
  },
};
const actions = {
	 callupdateSchema({commit},loginData){
    console.log("data",loginData)
      return new Promise((resolve, reject) => {
      var params = new URLSearchParams();
    params.append("email", loginData.email);
    params.append("password", loginData.password);
    //let url = 'http://192.168.1.53/dia-project/dia-backend/public/api/login'
    //let url = 'http://192.168.1.53/NU/dia-backend/public/api/login'
    let url = 'http://192.168.1.53/NU/backend/public/api/login'

    //Axios.post(Userinfo_LoginUrl, params)

    Axios.post(url, params)
      .then(function(response) {
        console.log('token = ', response.data.accessToken);
        localStorage.setItem('accessToken', response.data.accessToken);
        if (response.data.status == "success") {
          commit("SET_LOGIN_DATA",loginData);
          resolve();
        } else {
          //commit("SET_MESSAGE", response.data.msg);
          reject();
        }
      })
      .catch(function(error) {
        console.log(error);
        reject();
      });
  })




      // console.log("Email - :", loginData )
      // commit("SET_LOGIN_DATA",loginData);
   
    },
};
export default {
  state,
  getters,
  actions,
  mutations
};