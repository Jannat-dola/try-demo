export const filePath = "http://192.168.1.62/NU/backend/file_resource/photo/";
export const preFix = "http://192.168.1.53/NU/backend/public/api/";
export const rootPath = "http://192.168.1.62/NU/backend";

export const UtilService = require('./services/utility.services');
export const Options = UtilService.getHeaderOptions();


export const loginUrl = preFix + "nuproject/login";

export const Userinfo_FetchUrl = preFix + "user";
export const Userinfo_SaveUrl = preFix + "user";
export const Userinfo_DeleteUrl = preFix + "user";
export const Userinfo_UpdateUrl = preFix + "user";
export const Userinfo_GetItemUrl = preFix + "Userinfo/GetItem";
export const Userinfo_GetViewUrl = preFix + "Userinfo/GetViewData";
export const Userinfo_LoginUrl = preFix + "login";

//----------user api---------------
// export const api1 = preFix +'user';
// export const api2 = "http://192.168.1.53/NU/public/api/user";

