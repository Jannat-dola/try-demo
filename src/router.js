import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  //base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () =>
              import(/* webpackChunkName: "about" */ "./layouts/module.vue"),
      children: [
          {
            path: "home",
            name: "home",
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () =>
              import(/* webpackChunkName: "about" */ "./views/Home.vue")
          },
          {
            path: "about",
            name: "about",
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () =>
              import(/* webpackChunkName: "about" */ "./views/About.vue")
          },
             {
                path: "table",
                name: "table",
                // route level code-splitting
                // this generates a separate chunk (about.[hash].js) for this route
                // which is lazy-loaded when the route is visited.
                component: () =>
                  import(/* webpackChunkName: "about" */ "./views/table.vue")
              },
              {
                path: "user",
                name: "user",
                component: () =>
                  import(/* webpackChunkName: "about" */ "./views/user.vue")
              },

      ]
    },

    {
      path: '/user',
      component: () =>
       import( "./layouts/userLayout.vue"),
       children: [

           {
              path: "login",
              name: "login",
              // route level code-splitting
              // this generates a separate chunk (about.[hash].js) for this route
              // which is lazy-loaded when the route is visited.
              component: () =>
                import(/* webpackChunkName: "about" */ "./views/login.vue")
            },
             {
                path: "signup",
                name: "signup",
                // route level code-splitting
                // this generates a separate chunk (about.[hash].js) for this route
                // which is lazy-loaded when the route is visited.
                component: () =>
                  import(/* webpackChunkName: "about" */ "./views/signup.vue")
              }


       ]

    },
    // {
    //   path: "/home",
    //   name: "home",
    //   component: Home
    // },
    // {
    //   path: "/about",
    //   name: "about",
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () =>
    //     import(/* webpackChunkName: "about" */ "./views/About.vue")
    // },
 
    
  ]
});
